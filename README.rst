Simplest Docker example
=======================

Build the Docker image with::

    docker build --tag python-hello .

and then run it with::

    docker run python-hello
